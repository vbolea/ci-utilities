#!/bin/sh

set -e

make_release () {
    exec release-cli create --name "Release $CI_COMMIT_TAG" --tag-name "$CI_COMMIT_TAG" "$@"
}

readonly urlbase="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_COMMIT_TAG}"

assets=""
case "$CI_COMMIT_TAG" in
    git-macos/v*)
        # Asset discovery.
        readonly macos_binary="$( ls kitware-ci-macos-git-*.tar )"

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG deployment tarball\",\"url\":\"$urlbase/$macos_binary\"}"
        ;;
    gitlab-runner/v*)
        # Change to where the binaries live.
        cd out/

        # Asset discovery.
        readonly linux_amd64_deb="$( ls gitlab-runner-*-amd64.deb )"
        readonly linux_arm64_deb="$( ls gitlab-runner-*-arm64.deb )"
        readonly linux_amd64_rpm="$( ls gitlab-runner-*-amd64.rpm )"
        readonly linux_arm64_rpm="$( ls gitlab-runner-*-arm64.rpm )"
        readonly macos_amd64="$( ls gitlab-runner-*-darwin-amd64 )"
        readonly macos_arm64="$( ls gitlab-runner-*-darwin-arm64 )"
        readonly windows_amd64="$( ls gitlab-runner-*-windows-amd64.exe  )"

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux amd64 .deb\",\"url\":\"$urlbase/$linux_amd64_deb\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux arm64 .deb\",\"url\":\"$urlbase/$linux_arm64_deb\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux amd64 .rpm\",\"url\":\"$urlbase/$linux_amd64_rpm\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux arm64 .rpm\",\"url\":\"$urlbase/$linux_arm64_rpm\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS amd64\",\"url\":\"$urlbase/$macos_amd64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS arm64\",\"url\":\"$urlbase/$macos_arm64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows amd64\",\"url\":\"$urlbase/$windows_amd64\"}"
        ;;
    sccache/v*)
        # Asset discovery.
        readonly linux_x86_64="$( ls sccache-*-x86_64-unknown-linux-musl )"
        readonly linux_aarch64="$( ls sccache-*-aarch64-unknown-linux-musl )"
        readonly macos_universal="$( ls sccache-*-universal-apple-darwin )"
        readonly windows_x86_64="$( ls sccache-*-x86_64-pc-windows-gnu.exe )"

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux aarch64\",\"url\":\"$urlbase/$linux_aarch64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64/arm64\",\"url\":\"$urlbase/$macos_universal\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}"
        ;;
    cargo2junit/v*)
        # Asset discovery.
        readonly linux_x86_64="$( ls cargo2junit-*-x86_64-unknown-linux-gnu )"

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}"
        ;;
    qt/v*)
        # Asset discovery.
        readonly macos_arm64="$( ls qt-*-macosx*-arm64.tar.xz )"

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS arm64\",\"url\":\"$urlbase/$macos_arm64\"}"
        ;;
    ninja/v*)
        # Asset discovery.
        readonly linux_aarch64="$( ls ninja-*-linux-aarch64 )"
        readonly linux_x86_64="$( ls ninja-*-linux-x86_64 )"
        readonly macos_universal="$( ls ninja-*-macos-universal )"
        readonly windows_x86_64="$( ls ninja-*-windows-x86_64.exe )"

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux aarch64\",\"url\":\"$urlbase/$linux_aarch64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64/arm64\",\"url\":\"$urlbase/$macos_universal\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}"
        ;;
    *)
        echo >&2 "Unknown release artifact set."
        exit 1
esac
