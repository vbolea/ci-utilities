#!/bin/sh

set -e

arch="$( uname -m )"
readonly arch

apk add git

. sccache/package-common.sh

apk add musl-dev

echo "Building for $arch-unknown-linux-musl"
cargo build --no-default-features --features=redis --release --target "$arch-unknown-linux-musl"
out="$GIT_CLONE_PATH/sccache-v$version-$arch-unknown-linux-musl"
cp "target/$arch-unknown-linux-musl/release/sccache" "$out"
strip "$out"
