# Keep in sync with the common script.
$git_url = 'https://github.com/Kitware/ninja.git'
$git_commit = '51db22c9ece4cb08f6c460b3b0257ce1a6fb5d8e' # v1.10.2.g51db2.kitware.jobserver-1
$version = '1.10.2.g51db2.kitware.jobserver-1'

git clone "$git_url" ninja/src
git -C ninja/src -c advice.detachedHead=false checkout "$git_commit"

# Build the x86_64 variant.
New-Item -Path "ninja-x86_64" -Type Directory
cd "ninja-x86_64"
cmake `
  -GNinja `
  -DCMAKE_BUILD_TYPE=Release `
  -DBUILD_TESTING=OFF `
  "../ninja/src"
cmake --build .
cd ..

mv ninja-x86_64/ninja.exe "ninja-$version-windows-x86_64.exe"
