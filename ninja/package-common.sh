# Keep in sync with the Windows script.
readonly git_url='https://github.com/Kitware/ninja.git'
readonly git_commit='51db22c9ece4cb08f6c460b3b0257ce1a6fb5d8e' # v1.10.2.g51db2.kitware.jobserver-1
readonly version='1.10.2.g51db2.kitware.jobserver-1'

git clone "$git_url" ninja/src
git -C ninja/src -c advice.detachedHead=false checkout "$git_commit"
